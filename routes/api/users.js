const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const { body, validationResult } = require("express-validator");
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
/**
 * @route POST api/users
 * @desc Register users
 * @access Public
 */
router.post(
  "/",
  [
    body("name", "Name is required!!!").not().isEmpty(),
    body("email", "Please include a validate email address!!!").isEmail(),
    body("password", "Please enter with 6 or more character!!!").isLength({
      min: 6,
    }),
  ],
  async (req, res) => {
    try {
      const error = validationResult(req);
      if (!error.isEmpty()) {
        return res.status(400).json({ errors: error.array() });
      }

      const { name, email, password } = req.body;
      let user = await User.findOne({ email });

      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User already exists!!!" }] });
      }
      const avatar = gravatar.url(email, {
        s: "200",
        r: "pg",
        d: "mm",
      });

      user = new User({
        name,
        email,
        avatar,
        password,
      });

      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);
      //Save user
      await user.save();
      let payload = {
        user: {
          id: user.id,
        },
      };
      //jwt token
      jwt.sign(
        payload,
        config.get("jwtSecret"),
        {
          expiresIn: 360000,
        },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (error) {
      console.log(`Error @register user: ${error.message}`);
      res.status(500).send("Server Error");
    }
  }
);

module.exports = router;
