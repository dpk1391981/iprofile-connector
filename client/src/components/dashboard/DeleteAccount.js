import React, { Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { deleteMyAccount } from "../../actions/profile";
import PropTypes from "prop-types";

const DeleteAccount = ({ deleteMyAccount, history, confirmAlert }) => {
  const deleteAccount = () => {
    confirmAlert({
      title: "Confirm to submit",
      message: "Are you sure to do this.",
      buttons: [
        {
          label: "Yes",
          onClick: () => deleteMyAccount(history),
        },
        {
          label: "No",
        },
      ],
    });
  };
  return (
    <Fragment>
      <div className="my-2">
        <button className="btn btn-danger" onClick={deleteAccount}>
          <i className="fas fa-user-minus"></i>
          Delete My Account
        </button>
      </div>
    </Fragment>
  );
};

DeleteAccount.propTypes = {
  deleteMyAccount: PropTypes.func.isRequired,
};

export default connect(null, { deleteMyAccount })(withRouter(DeleteAccount));
