import React, { Fragment } from "react";
import { Link, withRouter } from "react-router-dom";
import Moment from "react-moment";
import { connect } from "react-redux";
import { deleteEducation } from "../../actions/profile";
import PropTypes from "prop-types";

const Education = ({ education, deleteEducation, history, confirmAlert }) => {
  const onRemoveEducation = (exp_id) => {
    confirmAlert({
      title: "Confirm to submit",
      message: "Are you sure to do this.",
      buttons: [
        {
          label: "Yes",
          onClick: () => deleteEducation(exp_id, history),
        },
        {
          label: "No",
        },
      ],
    });
  };
  const educationContent = education.map((exp) => (
    <tr key={exp._id}>
      <td>{exp.school}</td>
      <td className="hide-sm">{exp.degree}</td>
      <td className="hide-sm">
        <Moment format="YYYY/MM/DD">{exp.from}</Moment>-{" "}
        {!exp.to ? "Now" : <Moment format="YYYY/MM/DD">{exp.to}</Moment>}
      </td>
      <td>
        <button
          className="btn btn-danger"
          onClick={() => onRemoveEducation(exp._id)}
        >
          Delete
        </button>
      </td>
    </tr>
  ));
  return (
    <Fragment>
      <h2 className="my-2">Education Credentials</h2>
      {education.length > 0 ? (
        <table className="table">
          <thead>
            <tr>
              <th>School</th>
              <th className="hide-sm">Degree</th>
              <th className="hide-sm">Years</th>
              <th />
            </tr>
          </thead>
          <tbody>{educationContent}</tbody>
        </table>
      ) : (
        <Link to="/add-education" className="btn btn-primary my-1">
          Add Education
        </Link>
      )}
    </Fragment>
  );
};

Education.propTypes = {
  education: PropTypes.array.isRequired,
  deleteEducation: PropTypes.func.isRequired,
};

export default connect(null, { deleteEducation })(withRouter(Education));
