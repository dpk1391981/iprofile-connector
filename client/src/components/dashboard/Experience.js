import React, { Fragment } from "react";
import { Link, withRouter } from "react-router-dom";
import Moment from "react-moment";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { deleteExperience } from "../../actions/profile";

const Experience = ({
  experience,
  deleteExperience,
  history,
  confirmAlert,
}) => {
  const onRemoveExperience = (exp_id) => {
    confirmAlert({
      title: "Confirm to submit",
      message: "Are you sure to do this.",
      buttons: [
        {
          label: "Yes",
          onClick: () => deleteExperience(exp_id, history),
        },
        {
          label: "No",
        },
      ],
    });
  };
  const experienceContent = experience.map((exp) => (
    <tr key={exp._id}>
      <td>{exp.company}</td>
      <td className="hide-sm">{exp.title}</td>
      <td className="hide-sm">
        <Moment format="YYYY/MM/DD">{exp.from}</Moment>-{" "}
        {!exp.to ? "Now" : <Moment format="YYYY/MM/DD">{exp.to}</Moment>}
      </td>
      <td>
        <button
          className="btn btn-danger"
          onClick={() => onRemoveExperience(exp._id)}
        >
          Delete
        </button>
      </td>
    </tr>
  ));
  return (
    <Fragment>
      <h2 className="my-2">Experience Credentials</h2>
      {experience.length > 0 ? (
        <table className="table">
          <thead>
            <tr>
              <th>Company</th>
              <th className="hide-sm">Title</th>
              <th className="hide-sm">Years</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{experienceContent}</tbody>
        </table>
      ) : (
        <Link to="/add-experience" className="btn btn-primary my-1">
          Add Experience
        </Link>
      )}
    </Fragment>
  );
};

Experience.propTypes = {
  experience: PropTypes.array.isRequired,
  deleteExperience: PropTypes.func.isRequired,
};

export default connect(null, { deleteExperience })(withRouter(Experience));
