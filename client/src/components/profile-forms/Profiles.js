import React, { Fragment, useEffect } from "react";
import { connect } from "react-redux";
import { getAllProfiles } from "../../actions/profile";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";

const Profiles = ({ getAllProfiles, profile: { profiles, loading } }) => {
  useEffect(() => {
    getAllProfiles();
  }, [loading]);

  const profilesContent =
    profiles &&
    profiles.map((profile) => (
      <div className="profile bg-light" key={profile._id}>
        <LazyLoadImage
          alt={profile.user.name}
          src={profile.user.avatar} // use normal <img> attributes as props
          className="round-img"
          effect="blur"
        />
        <div>
          <h2>{profile.user.name}</h2>
          <p>Developer at {profile.company}</p>
          <p>{profile.location}</p>
          <Link
            to={"/profile/user/" + profile.user._id}
            className="btn btn-primary"
          >
            View Profile
          </Link>
        </div>

        <ul>
          {profile.skills.map((sk, id) => (
            <li className="text-primary" key={id}>
              <i className="fas fa-check"></i> {sk}
            </li>
          ))}
        </ul>
      </div>
    ));
  return (
    <Fragment>
      <h1 className="large text-primary">Developers</h1>
      <p className="lead">
        <i className="fab fa-connectdevelop"></i> Browse and connect with
        developers
      </p>
      <div className="profiles">{profilesContent}</div>
    </Fragment>
  );
};

Profiles.propTypes = {
  profile: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  profile: state.profile,
});
export default connect(mapStateToProps, { getAllProfiles })(Profiles);
