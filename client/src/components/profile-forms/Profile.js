import React, { Fragment, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getProfileById } from "../../actions/profile";
import PropTypes from "prop-types";
import Moment from "react-moment";
import { LazyLoadImage } from "react-lazy-load-image-component";

const Profile = ({
  profile: { profile, loading },
  history,
  getProfileById,
}) => {
  const {
    location: { pathname },
  } = history;
  const profileId = pathname.split("/")[3];

  useEffect(() => {
    getProfileById(profileId);
  }, [loading]);

  const data = profile;

  const socialLink =
    data &&
    data.social &&
    Object.keys(data.social).map((key) => (
      <a
        href={`//${data.social[key].replace(/(^\w+:|^)\/\//, "")}`}
        target="_blank"
        key={key}
      >
        <i className={`fab fa-${key} fa-2x`}></i>
      </a>
    ));

  const skillsElement =
    data &&
    data.skills &&
    data.skills.map((sk, key) => (
      <div className="p-1" key={key}>
        <i className="fa fa-check"></i> {sk}
      </div>
    ));

  const experienceElement =
    data &&
    data.experience &&
    data.experience.map((ex, key) => (
      <div key={key}>
        <h3 className="text-dark">{ex.company}</h3>
        <p>
          {<Moment format="YYYY/MM/DD">{ex.from}</Moment>} -{" "}
          {ex.to ? <Moment format="YYYY/MM/DD">{ex.to}</Moment> : "now"}
        </p>
        <p>
          <strong>Position: </strong>
          {ex.title}
        </p>
        <p>
          <strong>Description: </strong>
          {ex.description}
        </p>
      </div>
    ));

  const educationElement =
    data &&
    data.education &&
    data.education.map((ed, key) => (
      <div key={key}>
        <h3>{ed.school}</h3>
        <p>
          {" "}
          {<Moment format="YYYY/MM/DD">{ed.from}</Moment>} -{" "}
          {ed.to ? <Moment format="YYYY/MM/DD">{ed.to}</Moment> : "now"}
        </p>
        <p>
          <strong>Degree: </strong>
          {ed.degree}
        </p>
        <p>
          <strong>Field Of Study: </strong>
          {ed.fieldofstudy}
        </p>
        <p>
          <strong>Description: </strong>
          {ed.description}
        </p>
      </div>
    ));
  return (
    <Fragment>
      <Link to="/profile" className="btn btn-light">
        Back To Profiles
      </Link>

      <div className="profile-grid my-1">
        <div className="profile-top bg-primary p-2">
          <LazyLoadImage
            alt={data && data.user.name}
            src={data && data.user.avatar} // use normal <img> attributes as props
            className="round-img my-1"
            effect="blur"
          />
          <h1 className="large">{data && data.user.name}</h1>
          <p className="lead">Developer at {data && data.company}</p>
          <p>{data && data.location}</p>
          <div className="icons my-1">{socialLink}</div>
        </div>

        <div className="profile-about bg-light p-2">
          <h2 className="text-primary">{data && data.user.name}'s Bio</h2>
          <p>{data && data.bio}</p>
          <div className="line"></div>
          <h2 className="text-primary">Skill Set</h2>
          <div className="skills">{skillsElement}</div>
        </div>

        <div className="profile-exp bg-white p-2">
          <h2 className="text-primary">Experience</h2>
          {experienceElement}
        </div>

        <div className="profile-edu bg-white p-2">
          <h2 className="text-primary">Education</h2>
          {educationElement}
        </div>

        {/* //ToDO;  github repo
             <div className="profile-github">
          <h2 className="text-primary my-1">
            <i className="fab fa-github"></i> Github Repos
          </h2>
          <div className="repo bg-white p-1 my-1">
            <div>
              <h4>
                <a href="#" target="_blank" rel="noopener noreferrer">
                  Repo One
                </a>
              </h4>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Repellat, laborum!
              </p>
            </div>
            <div>
              <ul>
                <li className="badge badge-primary">Stars: 44</li>
                <li className="badge badge-dark">Watchers: 21</li>
                <li className="badge badge-light">Forks: 25</li>
              </ul>
            </div>
          </div>
          <div className="repo bg-white p-1 my-1">
            <div>
              <h4>
                <a href="#" target="_blank" rel="noopener noreferrer">
                  Repo Two
                </a>
              </h4>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Repellat, laborum!
              </p>
            </div>
            <div>
              <ul>
                <li className="badge badge-primary">Stars: 44</li>
                <li className="badge badge-dark">Watchers: 21</li>
                <li className="badge badge-light">Forks: 25</li>
              </ul>
            </div>
          </div>
        </div> */}
      </div>
    </Fragment>
  );
};

Profile.propTypes = {
  getProfileById: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  profile: state.profile,
});
export default connect(mapStateToProps, { getProfileById })(
  withRouter(Profile)
);
