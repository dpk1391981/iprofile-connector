import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import Moment from "react-moment";
import { connect } from "react-redux";
import { addLikes, removeLikes, removePost } from "../../actions/post";

const PostItem = ({
  auth,
  post,
  addLikes,
  removeLikes,
  removePost,
  showActions,
}) => {
  const { _id, name, avatar, text, date, likes, comments, user } = post;
  const showActionsDiv = showActions && (
    <Fragment>
      <button
        onClick={(e) => addLikes(_id)}
        type="button"
        className="btn btn-light"
      >
        <i className="fas fa-thumbs-up"></i>
        <span> {likes.length}</span>
      </button>
      <button
        onClick={(e) => removeLikes(_id)}
        type="button"
        className="btn btn-light"
      >
        <i className="fas fa-thumbs-down"></i>
      </button>
      <Link to={`/post/${_id}`} className="btn btn-primary">
        Discussion <span className="comment-count"> {comments.length}</span>
      </Link>
      {!auth.loading && user === auth.user._id && (
        <button
          onClick={(e) => removePost(_id)}
          type="button"
          className="btn btn-danger"
        >
          <i className="fas fa-times"></i>
        </button>
      )}
    </Fragment>
  );
  return (
    <Fragment>
      <div className="posts">
        <div className="post bg-white p-1 my-1">
          <div>
            <Link to={`/profile/user/${user}`}>
              <img className="round-img" src={avatar} alt="" />
              <h4>{name}</h4>
            </Link>
          </div>
          <div>
            <p className="my-1">{text}</p>
            <p className="post-date">
              Posted on <Moment format="DD/MM/YYYY">{date}</Moment>
            </p>
            {showActionsDiv}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

PostItem.defaultProps = {
  showActions: true,
};
PostItem.propTypes = {
  post: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, { addLikes, removeLikes, removePost })(
  PostItem
);
