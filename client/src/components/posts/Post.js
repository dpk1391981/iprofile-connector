import React, { Fragment, useEffect } from "react";
import { Link } from "react-router-dom";
import Spinner from "../layouts/Spinner";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getPost } from "../../actions/post";
import PostItem from "../posts/PostItem";
import CommentForm from "../posts/CommentForm";
import CommentItem from "../posts/CommentItem";

const Post = ({ post: { post, loading }, getPost, match }) => {
  useEffect(() => {
    getPost(match.params.id);
  }, true);

  return loading && !post ? (
    <Spinner />
  ) : (
    <Fragment>
      <Link to="/posts" className="btn">
        Back To Posts
      </Link>
      {post && <PostItem post={post} showActions={false} />}

      {post && <CommentForm postId={post._id} />}
      <div className="comments">
        {post &&
          post.comments.length > 0 &&
          post.comments.map((comment) => (
            <CommentItem postId={post._id} comment={comment} />
          ))}
      </div>
    </Fragment>
  );
};

Post.propTypes = {
  getPost: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  post: state.post,
});
export default connect(mapStateToProps, { getPost })(Post);
