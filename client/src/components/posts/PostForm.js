import React, { useState, Fragment } from "react";
import { createPost } from "../../actions/post";
import { connect } from "react-redux";

const PostForm = ({ createPost }) => {
  const [text, setText] = useState("");
  const onChangeText = (e) => {
    setText({
      [e.target.name]: e.target.value.trim(),
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    createPost(text);
    setText({ text: "" });
  };
  return (
    <Fragment>
      <div className="post-form">
        <div className="bg-primary p">
          <h3>Say Something...</h3>
        </div>
        <form className="form my-1" onSubmit={(e) => onSubmit(e)}>
          <textarea
            value={text["text"]}
            onChange={(e) => onChangeText(e)}
            name="text"
            cols="30"
            rows="5"
            type="text"
            placeholder="Create a post"
            required
          />
          <input type="submit" className="btn btn-dark my-1" value="Submit" />
        </form>
      </div>
    </Fragment>
  );
};

export default connect(null, { createPost })(PostForm);
