import React, { Fragment } from "react";
import spinner from "./spiner.gif";

export default function () {
  return (
    <Fragment>
      <img
        src={spinner}
        style={{ width: "200px", margin: "auto", display: "block" }}
        alt="Loading..."
      />
    </Fragment>
  );
}
