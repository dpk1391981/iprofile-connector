import React, { Fragment } from "react";
import { Link, Redirect } from "react-router-dom";
import { logout } from "../../actions/auth";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const Navbar = ({
  auth: { isAuthenticated, loading },
  profile: { profile },
  logout,
}) => {
  console.log(profile);
  let guestLink = (
    <ul>
      <li>
        <i className="fas fa-user"></i>
        <Link to="/profile">Developers</Link>
      </li>
      <li>
        <Link to="/register">Register</Link>
      </li>
      <li>
        <Link to="/login">Login</Link>
      </li>
    </ul>
  );
  let authLink = (
    <ul>
      <li>
        <i className="fas fa-user"></i>
        <Link to="/profile">Developers</Link>
      </li>
      <li>
        <Link to="/posts">
          <i className="fas fa-sticky-note"></i>
          <span className="hide-sm">Post</span>
        </Link>
      </li>
      <li>
        <Link to="/dashboard">
          <i className="fas fa-user"></i>
          <span className="hide-sm"> Dashboard</span>
        </Link>
      </li>
      <li>
        <a href="#!" onClick={logout}>
          <i className="fas fa-sign-out-alt" />
          <span className="hide-sm">Logout</span>
        </a>
      </li>
    </ul>
  );
  let navBar = !loading && (
    <Fragment>{isAuthenticated ? authLink : guestLink}</Fragment>
  );

  return (
    <nav className="navbar bg-dark">
      <h1>
        <Link to="/">
          <i className="fas fa-code"></i> iProfileConnector
        </Link>
      </h1>
      {navBar}
    </nav>
  );
};

Navbar.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  profile: state.profile,
});
export default connect(mapStateToProps, { logout })(Navbar);
