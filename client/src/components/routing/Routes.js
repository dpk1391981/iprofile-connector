import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom";

import Dashboard from "../dashboard/Dashboard";
import Login from "../auth/Login";
import Register from "../auth/Register";
import Alert from "../layouts/Alert";
import CreateProfile from "../profile-forms/CreateProfile";
import EditProfile from "../profile-forms/EditProfile";
import AddExperience from "../profile-forms/AddExperience";
import AddEducation from "../profile-forms/AddEducation";
import Profiles from "../profile-forms/Profiles";
import Profile from "../profile-forms/Profile";
import Posts from "../posts/Posts";
import Post from "../posts/Post";
import PrivateRoute from "../routing/PrivateRoute";
import NotFound from "../layouts/NotFound";

const Routes = () => {
  return (
    <Fragment>
      <section className="container">
        <Alert />
        <Switch>
          <Route exact path="/profile" component={Profiles} />
          <Route exact path="/profile/user/:id" component={Profile} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <PrivateRoute exact path="/dashboard" component={Dashboard} />
          <PrivateRoute
            exact
            path="/create-profile"
            component={CreateProfile}
          />
          <PrivateRoute exact path="/edit-profile" component={EditProfile} />
          <PrivateRoute
            exact
            path="/add-experience"
            component={AddExperience}
          />
          <PrivateRoute exact path="/add-education" component={AddEducation} />
          <PrivateRoute exact path="/posts" component={Posts} />
          <PrivateRoute exact path="/post/:id" component={Post} />
          <Route component={NotFound} />
        </Switch>
      </section>
    </Fragment>
  );
};

export default Routes;
