import {
  GET_PROFILE,
  PROFILE_ERROR,
  PROFILE_UPDATED,
  REMOVE_EDUCATION,
  REMOVE_EXPERIENCE,
  DELETE_ACCOUNT,
  GET_ALL_PROFILE,
  PROFILE_CLEAR,
} from "../actions/type";

const initialState = {
  profile: null,
  profiles: [],
  repos: [],
  loading: true,
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_PROFILE:
    case PROFILE_UPDATED:
    case REMOVE_EXPERIENCE:
    case REMOVE_EDUCATION:
      return {
        ...state,
        profile: payload,
        loading: false,
      };

    case GET_ALL_PROFILE:
      return {
        ...state,
        profiles: payload,
        loading: false,
      };

    case PROFILE_ERROR:
    case DELETE_ACCOUNT:
      return {
        ...state,
        error: payload,
        loading: false,
      };

    case PROFILE_CLEAR:
      return {
        ...state,
        loading: false,
        profile: null,
        profiles: [],
        repos: [],
      };

    default:
      return state;
  }
};
