import axios from "axios";
import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  CLEAR_PROFILE,
  PROFILE_CLEAR,
} from "./type";
import { setAlert } from "./alert";
import setAuthToken from "./../utils/setAuthToken";

export const loadedUser = () => async (dispatch) => {
  if (localStorage.token) {
    setAuthToken(localStorage.token);
  }
  try {
    const { data } = await axios.get("/api/auth");
    dispatch({
      type: USER_LOADED,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: AUTH_ERROR,
    });
  }
};
export const register = ({ name, email, password }) => async (dispatch) => {
  const config = {
    headers: {
      "content-type": "application/json",
    },
  };
  const body = JSON.stringify({ name, email, password });
  try {
    const res = await axios.post("/api/users", body, config);
    console.log(res);
    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data,
    });
    dispatch(setAlert("Register Successfully", "success"));
    dispatch(loadedUser());
  } catch (error) {
    const errors = error.response.data.errors;
    // console.log(errors);
    if (errors) {
      errors.forEach((er) => dispatch(setAlert(er.msg, "danger")));
    }
    dispatch({
      type: REGISTER_FAIL,
    });
  }
};

export const login = (email, password) => async (dispatch) => {
  const config = {
    headers: {
      "content-type": "application/json",
    },
  };
  const body = JSON.stringify({ email, password });
  try {
    const res = await axios.post("/api/auth", body, config);
    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data,
    });
    dispatch(loadedUser());
  } catch (error) {
    const errors = error.response.data.errors;
    // console.log(errors);
    if (errors) {
      errors.forEach((er) => dispatch(setAlert(er.msg, "danger")));
    }
    dispatch({
      type: LOGIN_FAIL,
    });
  }
};

export const logout = () => async (dispatch) => {
  dispatch({
    type: LOGOUT_SUCCESS,
  });
  dispatch({
    type: CLEAR_PROFILE,
  });
  dispatch({
    type: PROFILE_CLEAR,
  });
};
