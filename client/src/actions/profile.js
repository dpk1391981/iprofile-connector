import axios from "axios";
import { setAlert } from "./alert";
import { logout } from "./auth";
import {
  GET_PROFILE,
  PROFILE_ERROR,
  PROFILE_UPDATED,
  REMOVE_EXPERIENCE,
  REMOVE_EDUCATION,
  DELETE_ACCOUNT,
  GET_ALL_PROFILE,
} from "./type";

import setAuthToken from "./../utils/setAuthToken";

//get profile
export const getCurrentProfile = () => async (dispatch) => {
  if (localStorage.token) {
    setAuthToken(localStorage.token);
  }
  try {
    let res = await axios.get("/api/profile/me");
    dispatch({
      type: GET_PROFILE,
      payload: res.data,
    });
  } catch (err) {
    if (err.response) {
      dispatch({
        type: PROFILE_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status },
      });
    } else {
      dispatch({
        type: PROFILE_ERROR,
      });
    }
  }
};

export const getAllProfiles = () => async (dispatch) => {
  try {
    let res = await axios.get("/api/profile");
    dispatch({
      type: GET_ALL_PROFILE,
      payload: res.data,
    });
  } catch (err) {
    if (err.response) {
      dispatch({
        type: PROFILE_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status },
      });
    } else {
      dispatch({
        type: PROFILE_ERROR,
      });
    }
  }
};

export const getProfileById = (id) => async (dispatch) => {
  try {
    let res = await axios.get(`/api/profile/user/${id}`);
    dispatch({
      type: GET_PROFILE,
      payload: res.data,
    });
  } catch (err) {
    if (err.response) {
      dispatch({
        type: PROFILE_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status },
      });
    } else {
      dispatch({
        type: PROFILE_ERROR,
      });
    }
  }
};

//create Profile
export const createProfile = (formData, history, edit = false) => async (
  dispatch
) => {
  try {
    const config = {
      headers: {
        "content-type": "application/json",
      },
    };
    let res = await axios.post("/api/profile", formData, config);
    dispatch({
      type: PROFILE_UPDATED,
      payload: res.data,
    });
    dispatch(
      setAlert(edit ? "Profile updated!" : "Profile Created!", "success")
    );
    if (!edit) {
      history.push("/dashboard");
    }
  } catch (err) {
    const errors = err.response.data.errors;
    // console.log(errors);
    if (errors) {
      errors.forEach((er) => dispatch(setAlert(er.msg, "danger")));
    }

    if (err.response) {
      dispatch({
        type: PROFILE_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status },
      });
    } else {
      dispatch({
        type: PROFILE_ERROR,
      });
    }
  }
};

//add expericence
export const addExperience = (formData, history) => async (dispatch) => {
  try {
    const config = {
      headers: {
        "content-type": "application/json",
      },
    };
    let res = await axios.put("/api/profile/experience", formData, config);
    dispatch({
      type: PROFILE_UPDATED,
      payload: res.data,
    });
    dispatch(setAlert("Experience Added!!!", "success"));
    history.push("/dashboard");
  } catch (err) {
    const errors = err.response.data.errors;
    // console.log(errors);
    if (errors) {
      errors.forEach((er) => dispatch(setAlert(er.msg, "danger")));
    }

    if (err.response) {
      dispatch({
        type: PROFILE_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status },
      });
    } else {
      dispatch({
        type: PROFILE_ERROR,
      });
    }
  }
};

//add education
export const addEducation = (formData, history) => async (dispatch) => {
  try {
    const config = {
      headers: {
        "content-type": "application/json",
      },
    };
    let res = await axios.put("/api/profile/education", formData, config);
    dispatch({
      type: PROFILE_UPDATED,
      payload: res.data,
    });
    dispatch(setAlert("Education Added!!!", "success"));
    history.push("/dashboard");
  } catch (err) {
    const errors = err.response.data.errors;
    // console.log(errors);
    if (errors) {
      errors.forEach((er) => dispatch(setAlert(er.msg, "danger")));
    }

    if (err.response) {
      dispatch({
        type: PROFILE_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status },
      });
    } else {
      dispatch({
        type: PROFILE_ERROR,
      });
    }
  }
};

//Delete experiecne

export const deleteExperience = (exp_id, history) => async (dispatch) => {
  try {
    let res = await axios.delete(`/api/profile/experience/${exp_id}`);
    dispatch({
      type: REMOVE_EXPERIENCE,
      payload: res.data,
    });
    dispatch(setAlert("Experience Removed!!!", "success"));
    history.push("/dashboard");
  } catch (err) {
    const errors = err.response.data.errors;
    // console.log(errors);
    if (errors) {
      errors.forEach((er) => dispatch(setAlert(er.msg, "danger")));
    }
  }
};

//Delete education

export const deleteEducation = (edu_id, history) => async (dispatch) => {
  try {
    let res = await axios.delete(`/api/profile/education/${edu_id}`);
    dispatch({
      type: REMOVE_EDUCATION,
      payload: res.data,
    });
    dispatch(setAlert("Education Removed!!!", "success"));
    history.push("/dashboard");
  } catch (err) {
    const errors = err.response.data.errors;
    // console.log(errors);
    if (errors) {
      errors.forEach((er) => dispatch(setAlert(er.msg, "danger")));
    }
  }
};

//Delete my account

export const deleteMyAccount = (history) => async (dispatch) => {
  try {
    let res = await axios.delete(`/api/profile`);
    dispatch({
      type: DELETE_ACCOUNT,
      payload: res.data,
    });
    dispatch(logout());
    dispatch(setAlert("Account Removed!!!", "success"));
    history.push("/dashboard");
  } catch (err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach((er) => dispatch(setAlert(er.msg, "danger")));
    }
  }
};
