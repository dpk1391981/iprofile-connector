const express = require("express");
const app = express();
const path = require("path");
const ROUTES = {
  users: require("./routes/api/users"),
  auth: require("./routes/api/auth"),
  profile: require("./routes/api/profile"),
  posts: require("./routes/api/posts"),
};
const connectDB = require("./config/db");

// const setUpProxy = require("./middleware/setUpProxy");

// setUpProxy(app);
//connect DB
connectDB();

//Initate body parser
app.use(express.json({ extended: false }));

//Define routes
app.use("/api/users", ROUTES["users"]);
app.use("/api/auth", ROUTES["auth"]);
app.use("/api/profile", ROUTES["profile"]);
app.use("/api/posts", ROUTES["posts"]);

if (process.env.NODE_ENV === "production") {
  //set static folder
  app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}
//server start
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server started @ ${PORT}`);
});
